﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using VSJira.Core;

namespace VSJira.Wpf
{
    public class MockVisualStudioServices : IVisualStudioServices
    {
        public event EventHandler<OptionsChangedEventArgs> OptionsChanged;

        public IJiraOptions GetConfigurationOptions()
        {
            return new JiraOptions()
            {
                Theme = VSJiraTheme.Default,
                OpenIssueMode = VSJiraOpenIssueMode.InVisualStudio,
                MaxIssuesPerRequest = 100,
                JiraServers = new JiraServerInfo[]
                {
                    new JiraServerInfo("Live", "https://farmas.atlassian.net", "admin"),
                    new JiraServerInfo("Test2", "http://foo.bar2", "myuser2"),
                }
            };
        }

        public void ShowOptionsPage()
        {
            MessageBox.Show("Show Options Page");
        }

        public void ShowJiraIssueToolWindow(VSJira.Core.UI.JiraIssueViewModel viewModel, VSJiraServices services)
        {
            var window = new JiraIssueWindow(viewModel, services);
            window.Show();
        }

        public void NotifyOptionsChanged(IJiraOptions options)
        {
            var args = new OptionsChangedEventArgs(options);
            OptionsChanged?.Invoke(this, args);
        }

        public void SetPendingChangesComment(string comment)
        {
            MessageBox.Show(comment);
        }

        public void ShowJiraIssueInInternalBrowser(string url)
        {
            MessageBox.Show("URL: " + url);
        }
    }
}
