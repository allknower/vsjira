﻿using Atlassian.Jira.Remote;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Atlassian.Jira;
using Newtonsoft.Json.Linq;
using RestSharp;
using System.Threading;

namespace VSJira.Core
{
    public class TestJiraRestClient : IJiraRestClient
    {
        public RestClient RestSharpClient
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        public JiraRestClientSettings Settings
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        public string Url
        {
            get
            {
                return "http://testjira";
            }
        }

        public Task<IRestResponse> ExecuteRequestAsync(IRestRequest request, CancellationToken token = default(CancellationToken))
        {
            throw new NotImplementedException();
        }

        public Task<JToken> ExecuteRequestAsync(Method method, string resource, object requestBody = null, CancellationToken toke = default(CancellationToken))
        {
            throw new NotImplementedException();
        }

        public Task<T> ExecuteRequestAsync<T>(Method method, string resource, object requestBody = null, CancellationToken token = default(CancellationToken))
        {
            throw new NotImplementedException();
        }
    }
}
