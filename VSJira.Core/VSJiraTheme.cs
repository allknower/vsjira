﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VSJira.Core
{
    public enum VSJiraTheme
    {
        Light = 0,
        Dark = 1,
        Default = 2147483647
    }
}
