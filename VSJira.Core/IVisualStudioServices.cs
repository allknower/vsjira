﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VSJira.Core.UI;

namespace VSJira.Core
{
    public class OptionsChangedEventArgs : EventArgs
    {
        public IJiraOptions Options { get; private set; }

        public OptionsChangedEventArgs(IJiraOptions options)
        {
            this.Options = options;
        }
    }

    public interface IVisualStudioServices
    {
        void SetPendingChangesComment(string comment);

        IJiraOptions GetConfigurationOptions();
        void ShowOptionsPage();
        void ShowJiraIssueToolWindow(JiraIssueViewModel viewModel, VSJiraServices services);
        void ShowJiraIssueInInternalBrowser(string url);
        event EventHandler<OptionsChangedEventArgs> OptionsChanged;
        void NotifyOptionsChanged(IJiraOptions options);
    }
}
