﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VSJira.Core.UI
{
    public class JiraIssueLinksUserControlViewModel : JiraBackedViewModel
    {
        public ObservableCollection<JiraIssueLinkViewModel> IssueLinks { get; set; }

        public JiraIssueLinksUserControlViewModel(VSJiraServices services)
            : base(services)
        {
            IssueLinks = new ObservableCollection<JiraIssueLinkViewModel>();
        }
    }
}
