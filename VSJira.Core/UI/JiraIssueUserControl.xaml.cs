﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace VSJira.Core.UI
{
    /// <summary>
    /// Interaction logic for JiraIssueUserControl.xaml
    /// </summary>
    public partial class JiraIssueUserControl : UserControl, IDisposable
    {
        public static readonly DependencyProperty IssueViewModelProperty = DependencyProperty.Register("IssueViewModel", typeof(JiraIssueViewModel), typeof(JiraIssueUserControl), new PropertyMetadata(OnIssueViewModelChanged));

        private readonly ThemeManager _themeManager;
        private JiraIssueUserControlViewModel _viewModel;

        public JiraIssueUserControl() : this(new JiraIssueUserControlViewModel()) { }

        public JiraIssueUserControl(JiraIssueUserControlViewModel viewModel)
        {
            InitializeComponent();
            this.Language = XmlLanguage.GetLanguage(Thread.CurrentThread.CurrentCulture.Name);
            this._themeManager = new ThemeManager(this.Resources);

            this.DataContext = viewModel;

            this._viewModel = viewModel;

            this.InwardIssueLinks.Content = new JiraIssueLinksUserControl(viewModel.InwardIssueLinks);
            this.OutwardIssueLinks.Content = new JiraIssueLinksUserControl(viewModel.OutwardIssueLinks);

            viewModel.Initialized += ViewModel_Initialized;
        }

        public JiraIssueViewModel IssueViewModel
        {
            get { return (JiraIssueViewModel)GetValue(IssueViewModelProperty); }
            set { SetValue(IssueViewModelProperty, value); }
        }

        private static void OnIssueViewModelChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var control = d as JiraIssueUserControl;
            var model = e.NewValue as JiraIssueViewModel;
            if (model != null && control != null)
            {
                control._viewModel.Initialize(model, model._services);
                control._viewModel.CommentsPager = null;
                control.Pages.SelectedIndex = 0;
                control.DataContext = control._viewModel;
            }
        }

        void ViewModel_Initialized(object sender, EventArgs e)
        {
            // set the initial theme of the control.
            var theme = _viewModel.Services.VisualStudioServices.GetConfigurationOptions().Theme;
            _themeManager.Apply(theme);

            // subscribe to changes to update the theme.
            this._viewModel.Services.VisualStudioServices.OptionsChanged += VisualStudioServices_OptionsChanged;
        }

        void VisualStudioServices_OptionsChanged(object sender, OptionsChangedEventArgs e)
        {
            _themeManager.Apply(e.Options.Theme);
        }

        public void Dispose()
        {
            if (this._viewModel != null && this._viewModel.Services != null)
            {
                this._viewModel.Services.VisualStudioServices.OptionsChanged -= VisualStudioServices_OptionsChanged;
            }
        }

        private async void TabControl_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (_viewModel?.Issue == null) return;

            if (e.OriginalSource is TabControl)
            {
                var tabItem = e.AddedItems.Cast<TabItem>().FirstOrDefault();

                if (tabItem != null && this._viewModel != null)
                {
                    var header = tabItem.Header.ToString();

                    if (header.Equals("Comments", StringComparison.OrdinalIgnoreCase) && this._viewModel.CommentsPager == null)
                    {
                        // instantiate pager and request comments the first time the tab is selected.
                        this._viewModel.CommentsPager = new JiraCommentsPagerViewModel(this._viewModel.Issue, this._viewModel.Services);
                        await this._viewModel.CommentsPager.ResetItemsAsync();
                    }
                    else if (header.Equals("Attachments", StringComparison.OrdinalIgnoreCase))
                    {
                        await this._viewModel.LoadAttachmentsAsync();
                    }
                    else if (header.Equals("Work Logs", StringComparison.OrdinalIgnoreCase))
                    {
                        await this._viewModel.LoadWorklogsAsync();
                    }
                    else if (header.Equals("Links", StringComparison.OrdinalIgnoreCase))
                    {
                        await this._viewModel.LoadIssueLinksAsync();
                    }
                }
            }
        }
    }
}
