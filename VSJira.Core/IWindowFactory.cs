﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VSJira.Core
{
    public interface IWindowFactory
    {
        void Show(object viewModel);
        void ShowDialog(object viewModel);
        void ShowMessageBox(string text, Exception ex = null);
    }
}
