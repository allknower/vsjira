﻿using EnvDTE;
using Microsoft.TeamFoundation.Client;
using Microsoft.TeamFoundation.Controls;
using Microsoft.TeamFoundation.Controls.WPF.TeamExplorer;
using Microsoft.TeamFoundation.VersionControl.Client;
using Microsoft.VisualStudio.Shell;
using Microsoft.VisualStudio.Shell.Interop;
using Microsoft.VisualStudio.TeamFoundation;
using System;
using System.Linq;
using System.Windows;
using VSJira.Core;
using VSJira.Core.UI;

namespace VSJira.Package
{
    public class VisualStudioServices : IVisualStudioServices
    {
        private readonly DTE _env;
        private readonly VSJiraPackage _package;

        public event EventHandler<OptionsChangedEventArgs> OptionsChanged;

        public VisualStudioServices(DTE environment, VSJiraPackage package)
        {
            this._env = environment;
            this._package = package;
        }

        public void ShowOptionsPage()
        {
            this._package.ShowOptionPage(typeof(JiraOptionsDialogPage));
        }

        public IJiraOptions GetConfigurationOptions()
        {
            var result = new JiraOptions();
            var properties = this._env.get_Properties(JiraOptionsDialogPage.CategoryName, JiraOptionsDialogPage.PageName);

            if (properties != null)
            {
                Object[] serverObjects;
                if (TryGetProperty(properties, "JiraServers", out serverObjects))
                {
                    result.JiraServers = serverObjects.Cast<JiraServerInfo>().ToArray();
                }

                int maxIssuesPerRequest;
                if (TryGetProperty(properties, "MaxIssuesPerRequest", out maxIssuesPerRequest))
                {
                    result.MaxIssuesPerRequest = maxIssuesPerRequest;
                }

                string openIssueMethodStr;
                VSJiraOpenIssueMode openIssueMode = VSJiraOpenIssueMode.InBrowser;
                if (TryGetProperty(properties, "OpenIssueModeHidden", out openIssueMethodStr) && Enum.TryParse<VSJiraOpenIssueMode>(openIssueMethodStr, out openIssueMode))
                {
                    result.OpenIssueMode = openIssueMode;
                }

                string themeStr;
                VSJiraTheme themeEnum = VSJiraTheme.Light;
                if (TryGetProperty(properties, "ThemeHidden", out themeStr) && Enum.TryParse<VSJiraTheme>(themeStr, out themeEnum))
                {
                    result.Theme = themeEnum;
                }
            }

            return result;
        }

        private bool TryGetProperty<T>(Properties properties, string propertyName, out T value)
        {
            value = default(T);
            try
            {
                value = (T)properties.Item(propertyName).Value;
            }
            catch
            {
                // no-op
            }

            return value != null;
        }

        public void ShowJiraIssueToolWindow(Core.UI.JiraIssueViewModel viewModel, VSJiraServices services)
        {

            this._package.ShowJiraIssueToolWindow(viewModel, services);
        }

        public void ShowJiraIssueInInternalBrowser(string url)
        {
            var browserService = _package.GetService(typeof(IVsWebBrowsingService)) as IVsWebBrowsingService;
            if (browserService == null) return;
            try
            {
                IVsWindowFrame ppFrame;
                // passing 0 to the NavigateFlags allows the browser service to reuse open instances of the internal browser.
                browserService.Navigate(url, 1, out ppFrame);
            }
            catch
            {
                // if the process could not be started, show an error.
                MessageBox.Show("Cannot launch this url.", "Extension Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        public void NotifyOptionsChanged(IJiraOptions options)
        {
            var args = new OptionsChangedEventArgs(options);
            OptionsChanged?.Invoke(this, args);
        }

        public void SetPendingChangesComment(string comment)
        {
            var teamExplorer = _package.GetService(typeof(ITeamExplorer)) as ITeamExplorer;
            if (teamExplorer == null) return;
            var pendingChangesPage = (TeamExplorerPageBase)teamExplorer.NavigateToPage(new Guid(TeamExplorerPageIds.PendingChanges), null);
            if (pendingChangesPage == null) return;

            var model = (IPendingCheckin)pendingChangesPage.Model;
            if (model == null) return;

            if (!String.IsNullOrWhiteSpace(model.PendingChanges.Comment))
            {
                var ans = MessageBox.Show("Rewrite comment?", "Question", MessageBoxButton.YesNoCancel, MessageBoxImage.Question);
                if (ans == MessageBoxResult.Cancel)
                    return;
                else if (ans == System.Windows.MessageBoxResult.No)
                    comment += " " + model.PendingChanges.Comment;
            }
            model.PendingChanges.Comment = comment;
        }
    }
}
